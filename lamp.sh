#!bin/bash

echo Installing LAMP Stack with PHPMYADMIN [APACHE, MARIA-DB, PHP]; sleep 2

sudo apt update

sudo apt upgrade -y

sudo apt-get install ca-certificates apt-transport-https software-properties-common -y

sudo apt-get install zip unzip -y

sudo apt install curl -y

sudo apt install git -y

sudo apt install wget -y



echo Installing Apache Server; sleep 2

curl -sSL https://packages.sury.org/apache2/README.txt | sudo bash -x

sudo apt update

sudo apt install apache2 -y



sudo apt-get update

echo Installing Maria-DB; sleep 2

curl -sS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | sudo bash

sudo apt update

sudo apt install mariadb-server mariadb-client -y



echo Installing PHP 8; sleep 2

sudo apt install apt-transport-https lsb-release ca-certificates -y


sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg

sudo apt-key adv --fetch-keys 'https://packages.sury.org/php/apt.gpg' > /dev/null 2>&1

sudo sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'



sudo apt update

sudo apt install php8.0 php8.0-cli php8.0-common php8.0-curl php8.0-gd php8.0-intl php8.0-mbstring php8.0-mysql php8.0-imagick php8.0-opcache php8.0-readline php8.0-xml php8.0-xsl php8.0-zip php8.0-bz2 php8.0-bcmath libapache2-mod-php8.0 -y




echo Ignore the 1st password promt, then press Y twice, then set new password, select Y for rest of the prompts; sleep 2

sudo mysql_secure_installation




echo Installing PHPMYADMIN manually; sleep 2

sudo mv phpmyadmin.conf ~

sudo mv config.inc.php ~







echo Downloading PHPMYADMIN; sleep 2

sudo wget -P /usr/share https://www.phpmyadmin.net/downloads/phpMyAdmin-latest-all-languages.zip

sudo unzip /usr/share/*phpMyAdmin*.zip -d /usr/share

sudo mv /usr/share/phpMyAdmin-*-all-languages /usr/share/phpmyadmin

sudo rm /usr/share/phpmyadmin.zip




sudo adduser $USER www-data

sudo chown -R www-data:www-data /usr/share/phpmyadmin




echo Copying Configuration Files; sleep 2


sudo mv ~/phpmyadmin.conf /etc/apache2/conf-available/

sudo a2enconf phpmyadmin

systemctl reload apache2

sudo rm /usr/share/phpmyadmin/config.sample.inc.php





sudo mv ~/config.inc.php /usr/share/phpmyadmin

sudo chmod 644 /usr/share/phpmyadmin/config.inc.php

sudo mkdir /var/lib/phpmyadmin

sudo mkdir /var/lib/phpmyadmin/tmp

sudo chown -R www-data:www-data /var/lib/phpmyadmin

sudo chown -R www-data:www-data /var/lib/phpmyadmin/tmp

sudo ln -s /usr/share/phpmyadmin /var/www/html

sudo systemctl reload apache2




echo Enter MARIA-DB password for fixing login and database issue- may ask for 2 times simultaneously; sleep 2



cd /usr/share/phpmyadmin/sql

mysql -u root -p < create_tables.sql 

mysql -u root -p -e 'GRANT SELECT, INSERT, DELETE, UPDATE ON phpmyadmin.* TO 'pma'@'localhost' IDENTIFIED BY "pmapassword"'

sudo systemctl reload apache2



echo Installing UFW Firewall...; sleep 2

sudo apt install ufw -y

echo Select y and press enter; sleep 1

sudo ufw enable

sudo ufw allow 22/tcp

sudo ufw allow 80/tcp

sudo ufw allow 443/tcp


echo Installation completed!!!; sleep 1

exit
